import QtWebService.Config 0.1

Config {
    listen.address: '*'
    listen.port: 8008
    server.name: 'QtWebService'

    contents: {
        '/': ':/frontend/',
    }

    cache: {
        'qml': true
    }

    deflate: {
        'video/*': false
        , 'image/*': false
    }

    daemons: [
        ':/backend/events/events_backend.qml'
    ]

    offlineStoragePath: './database/'
}
