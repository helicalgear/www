import QtWebService.HTML 5.0
import QtQml.Models 2.1

ListModel {
    ListElement {
        href: "http://asciimw.jp/search/mode/item/cd/311839700000"
        image: "http://asciimw.jp/search/img-item/978-4-04-891512-0.jpg"
    }
    ListElement {
        href: "http://www.shoeisha.co.jp/book/detail/9784798156620"
        image: "http://www.shoeisha.co.jp/static/book/cover/9784798156620.jpg"
    }
    ListElement {
    	href: "https://www.amazon.co.jp/dp/4777520609/"
	image: "https://images-na.ssl-images-amazon.com/images/I/41+XQgfN74L._SX350_BO1,204,203,200_.jpg"
    }
    ListElement {
        href: "https://relog.booth.pm/items/269707"
        image: "https://s.booth.pm/c/f_620/48d0f687-6442-4b33-83da-2de0c72f5a1b/i/269707/c600e72b-a45c-47fe-8d5a-9071e2ad68fd.jpg"
    }
    ListElement {
        href: "https://relog.booth.pm/items/478459"
	image: "https://s.booth.pm/c/f_620/48d0f687-6442-4b33-83da-2de0c72f5a1b/i/478459/ef8670ce-d134-4b37-a575-871b34706265.jpg"
    }
    ListElement {
        href: "https://relog.booth.pm/items/661176"
	image: "https://s.booth.pm/c/f_620/48d0f687-6442-4b33-83da-2de0c72f5a1b/i/661176/0ac14edf-b66d-4abf-93fe-5fde2e7067e6.jpg"
    }
    ListElement {
        href: "https://relog.booth.pm/items/810103"
        image: "https://s.booth.pm/c/f_620/48d0f687-6442-4b33-83da-2de0c72f5a1b/i/810103/19583a48-f7b4-4b8f-ac7d-92d0b49fea8b.jpg"
    }
    ListElement {
    	href: "https://booth.pm/ja/items/1561219"
	image: "https://booth.pximg.net/48d0f687-6442-4b33-83da-2de0c72f5a1b/i/1561219/68ee88cc-6476-406a-bf90-89cc7b3bb16d_base_resized.jpg"
    }
    ListElement {
        href: "https://bookwalker.jp/de180079de-5a07-40da-a51d-d28d6046339e/"
        image: "https://c.bookwalker.jp/9100412/t_700x780.jpg"
    }
    ListElement {
    	href: "https://booth.pm/ja/items/3045661"
	image: "https://booth.pximg.net/7287bb46-6dfb-43c3-94bd-827bc1141389/i/3045661/85b177b7-12c0-4efb-85cd-1f1b40c43291_base_resized.jpg"
    }
    ListElement {
    	href: "https://booth.pm/ja/items/1042093"
	image: "https://booth.pximg.net/7287bb46-6dfb-43c3-94bd-827bc1141389/i/1042093/cc65554f-b40c-49ef-b189-69a19d9e9398_base_resized.jpg"
    }
    ListElement {
    	href: "https://booth.pm/ja/items/1565906"
	image: "https://booth.pximg.net/7287bb46-6dfb-43c3-94bd-827bc1141389/i/1565906/2e80fe4d-b3da-473a-9300-86b7aa4f058a_base_resized.jpg"
    }
    ListElement {
    	href: "https://booth.pm/ja/items/1574684"
	image: "https://booth.pximg.net/b65deb01-ea4d-44c9-aba0-d8fa65ece8d5/i/1574684/94dc6cfe-2f2d-4b82-960c-4f38f9bcec87_base_resized.jpg"
    }
    ListElement {
    	href: "https://booth.pm/ja/items/1030838"
	image: "https://booth.pximg.net/39c04b76-81dc-467b-b1d6-0b7b32f61e00/i/1030838/91d05af0-16b6-452e-85f7-2ee1fb97f65d_base_resized.jpg"
    }
    ListElement {
    	href: "https://booth.pm/ja/items/1311049"
	image: "https://booth.pximg.net/39c04b76-81dc-467b-b1d6-0b7b32f61e00/i/1311049/8a4ce8c8-4631-407c-958d-b4c626248989_base_resized.jpg"
    }

}

